#include <iostream>
#include <unordered_map>
#include <cassert>
#include <cstring>
#include "dict.h"
#include "dictglobal.h"

#ifdef NDEBUG
    const bool debug = false;
#else
    const bool debug = true;
#endif

#define size_t int
using namespace std;

/**
 * Słowniki będziemy reprezentować jako nieuporządkowane mapy par zmiannych
 * typu string.
 */
typedef unordered_map<string, string> dictionary;

//bool if_init_dict_global = false;

/**
 * Zmienna do nadawania numerów (czyli kluczów w słowniku słowników)
 * kolejnym słownikom.
 */
unsigned long key = 1;

/**
 * Funkcja do wypisywania komunikatów diagnostycznych - jeśli program został
 * wywołany bez flagi oznaczającej brak takich komunikatów, to funkcja
 * wypisuje na cerr podany jej napis.
 */
void print_errors(string s) {
    if (debug) {
        cerr << s << endl;
    }
}

/**
 * Funkcja pomocnicza przy wypisywaniu tworząca komunikat o nieistnieniu
 * słownika od danym numerze.
 */
static inline string not_exist(unsigned long dict) {
    return "dict " + to_string(dict) + " does not exist";
}

/**
 * Funkcja pomocnicza przy wypisywaniu podająca właściwą nazwę słownika.
 */
static inline string name(unsigned long id) {
    return id == dict_global() ? "the Global Dictionary" : ("dict " + to_string(id));
}

/**
 * Typ słownika słowników - nieuporządkowana mapa trzymająca jako klucze
 * numery słowników a jako wartości same słowniki.
 */
using GlobalDictType = unordered_map<unsigned long, dictionary>;

/**
 * Funkcja zwracająca mapę ze wszystkimi słownikami. Jeśli jest użyta po raz
 * pierwszy to zaczyna od stworzenia słownika globalnego.
 */
GlobalDictType& getGlobalDict() {
    static GlobalDictType *ptr = nullptr;
    if (!ptr) {
        dictionary globalDict;
        ptr = new GlobalDictType({{0, globalDict}});
        print_errors("the Global Dictionary has been created");
    }
    return *ptr;
}

unsigned long dict_new() {
    auto& Dicts = getGlobalDict();
    if (debug) {
        assert(key != 0);
    }
    print_errors("dict_new()");

    dictionary newDict;
    Dicts.insert({key++, newDict});
    print_errors("dict_new: dict " + to_string(key - 1) + " has been created");
    return key - 1;
}

void dict_delete(unsigned long id) {
    auto& Dicts = getGlobalDict();
    print_errors("dict_delete(" + to_string(id) + ")");
    if (Dicts.count(id)) {

        if (id != dict_global()) {
            Dicts.erase(id);
        }
        string s = id == dict_global() ? "an attempt to remove the Global Dictionary"
                                       : ("dict " + to_string(id) + " has been deleted");
        print_errors("dict_delete: " + s);
    }
    else {
        print_errors("dict_delete: " + not_exist(id));
    }
}

size_t dict_size(unsigned long id) {
    auto& Dicts = getGlobalDict();
    print_errors("dict_size(" + to_string(id) + ")");

    if (Dicts.count(id)) {
        dictionary currDict = Dicts.find(id)->second;
        size_t result = currDict.size();
        print_errors("dict_size: " + name(id) + " contains " + to_string(result) + " element(s)");
        return result;

    } else {
        print_errors("dict_size: " + not_exist(id));
        return 0;
    }
}

void dict_insert(unsigned long id, const char* key, const char* value) {
    auto& Dicts = getGlobalDict();
    string sKey = key == NULL ? "NULL" : ("\"" + string(key) + "\"");
    string sValue = value == NULL ? "NULL" : ("\"" + string(value) + "\"");
    print_errors("dict_insert(" + to_string(id) + ", "
                 + sKey + ", " + sValue + ")");

    if (Dicts.count(id) && key != NULL && value != NULL) {
        string currKey = string(key);
        string currValue = string(value);
        pair<string, string> currPosition = make_pair(currKey, currValue);
        Dicts.find(id)->second.insert(currPosition);

        if (id != dict_global()) {
            print_errors("dict_insert: dict " + to_string(id)
                         + ", the pair (\"" + key + "\", \""
                         + value + "\")" + " has been inserted");
        }
        else if (id == dict_global()) {

                if (Dicts.find(id)->second.size() < MAX_GLOBAL_DICT_SIZE) {
                    print_errors("dict_insert: the Global Dictionary, the pair"
                                 + string(" (\"") + string(key) + "\", \""
                                 + string(value) + "\") has been inserted");
                }
                else {
                    print_errors("dict_insert: the Global Dictionary, an "
                                 + string("attempt to insert too many ")
                                 + "elements");
		}
        }
    }

    if (!Dicts.count(id)) {
        print_errors("dict_insert: " + not_exist(id));
    }

    if (key == NULL) {
        print_errors("dict_insert: " + name(id)
                     + ", an attempt to insert NULL key");
    }

    if (value == NULL) {
        print_errors("dict_insert: " + name(id)
                     + ", an attempt to insert NULL value");
    }
}

void dict_remove(unsigned long id, const char* key) {
    auto& Dicts = getGlobalDict();
    print_errors("dict_remove(" + to_string(id) + ", \"" + key + "\")");

    if (Dicts.count(id)) {
        if (Dicts.find(id)->second.count(key)) {
            Dicts.find(id)->second.erase(key);
            print_errors("dict_remove: " + name(id) + ", the key \""
                         + key + "\" has been removed");
        }
        else {
            print_errors("dict_remove: " + name(id)
                         + " does not contain the key \"" + key + "\"");
        }
    }
    else {
        print_errors("dict_remove: " + not_exist(id));
    }
}

/**
 * Pomocnicza funkcja używana w wywołaniu dict_find. Ma ona na celu zmiejszyć
 * ilość napisanego kodu, ponieważ w wywołaniu dict_find w przypadku gdy w
 * danym słowniku nie ma danego klucza, należy powtórzyć wyszukiwanie w
 * słowniku globalnym.
 */
static inline const char* dict_find_help(unsigned long id, unsigned long parent, const char* key) {
    auto& Dicts = getGlobalDict();

    if (Dicts.count(id) && Dicts.find(id)->second.count(key)) {
        string value = Dicts.find(id)->second.find(key)->second;
        print_errors("dict_find: " + name(parent) + ", the key \"" + key
                     + "\" has the value \"" + value + "\"");
        return value.c_str();
    }
    else {
        string if_true = "not found";
        string if_false = "not found, looking up the Global Dictionary";
        string looking_up = id == dict_global() ? if_true : if_false;
        print_errors("dict_find: the key \"" + string(key) + "\" "
                     + looking_up);
    }

    if (!Dicts.count(id)) {
        print_errors("dict_find: dict " + to_string(id) + " does not exist");
    }
    return NULL;
}

const char* dict_find(unsigned long id, const char* key) {
    print_errors("dict_find(" + to_string(id) + ", \"" + key + "\")");
    const char* ans = dict_find_help(id, id, key);

    if (ans == NULL && id != dict_global()) {
        ans = dict_find_help(dict_global(), id, key);
    }
    return ans;
}

void dict_clear(unsigned long id) {
    auto& Dicts = getGlobalDict();
    print_errors("dict_clear(" + to_string(id) + ")");

    if (Dicts.count(id)) {
        Dicts.find(id)->second.clear();
        print_errors("dict_clear: " + name(id) + " has been cleared");
    }
    else {
        print_errors("dict_clear: " + not_exist(id));
    }
}

void dict_copy(unsigned long src_id, unsigned long dst_id) {
    auto& Dicts = getGlobalDict();
    print_errors("dict_copy(" + to_string(src_id) + ", " + to_string(dst_id) + ")");
    if (Dicts.count(src_id) && Dicts.count(dst_id)) {

        size_t src_size = Dicts.find(src_id)->second.size();
        size_t dst_size = Dicts.find(dst_id)->second.size();
        if (dst_id == dict_global() && src_size + dst_size >  MAX_GLOBAL_DICT_SIZE) {
            print_errors("dict_copy: the Global Dictionary is too small "
                         + string("to copy all the elements"));
            return;
        }

        for (auto &val : Dicts.find(src_id)->second) {
            Dicts.find(dst_id)->second.insert(val);
        }
        print_errors("dict_copy: dict " + to_string(src_id)
                     + " has been copied to dict " + to_string(dst_id));
    }
    else {

        if (!Dicts.count(src_id)) {
            print_errors("dict_copy: " + not_exist(src_id));
        }

        if (!Dicts.count(dst_id)) {
            print_errors("dict_copy: " + not_exist(dst_id));
        }
    }
}
